# =====================================================
# DIBAWAH INI UNTUK CRAWL API COINMARKETCAP.COM/CURRENCIES
# =====================================================
import uuid
from datetime import datetime
from kafka import KafkaProducer
from faker import Faker
import requests
import schedule
import json
import time

fake = Faker()

def currencies():
    bagan = str(uuid.uuid4())
    headers = {
        'authority': 'api.coinmarketcap.com',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/97.0.4692.71 Mobile Safari/537.36',
        'content-type': 'application/json;charset=UTF-8',
        'accept': 'application/json, text/plain, */*',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
        'origin': 'https://coinmarketcap.com',
        'x-request-id': ''+bagan+'',
        'DNT': '1',
        'sec-fetch-site': 'same-site',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://coinmarketcap.com/',
        'accept-language': 'en-US,en;q=0.9',
    }
    tanggal = datetime.today().strftime('%Y-%m-%d')
    detik = datetime.today().strftime('%H:%M:%S')

    for i in range(0, 8):
        z = str(i)

        data = '{"start":' + z + ',"limit":20,"coinType":"all","current":true}'
        response = requests.post('https://api.coinmarketcap.com/data-api/v3/calendar/query', headers=headers, data=data,verify=False)
        data_response = json.loads(response.text)

        for val in range(len(data_response["data"])):
            for isi in data_response["data"][val]["eventsList"]:
                id = isi["name"][0]["id"]
                ids = json.dumps(id)
                data = '{"start":' + z + ',"limit":20,"coinType":"all","current":false,"coinIds":[' + ids + '],"timeStart":"2021-10-15T00:00:00.000Z","timeEnd":"' + tanggal + 'T' + detik + '.000Z"}'
                curr_response = requests.post('https://api.coinmarketcap.com/data-api/v3/calendar/query',
                                              headers=headers,
                                              data=data,verify=False)
                curr_data = json.loads(curr_response.text)

                for b in curr_data["data"]:
                    id = b["eventsList"][0]["name"][0]["id"]
                    title = b["eventsList"][0]["title"]
                    try:
                        contents = b["eventsList"][0].get("content")
                    except:
                        contents = " "
                    try:
                        tag = b["eventsList"][0]["tags"][0]
                    except:
                        tag = " "
                    link = b["eventsList"][0].get("originalSource")
                    like = b["eventsList"][0]["like"]
                    name = b["eventsList"][0]["name"][0]["name"]
                    slug = b["eventsList"][0]["name"][0]["slug"]
                    trends = b["eventsList"][0]["trending"]
                    confirmed = b["eventsList"][0]["confirmedByOfficials"]
                    times = b["timestamp"]
                    if tag == 1:
                        tag = "Release"
                    elif tag == 2:
                        tag = "Branding"
                    elif tag == 3:
                        tag = "Tokenomics"
                    elif tag == 4:
                        tag = "Exchange"
                    elif tag == 5:
                        tag = "Conference"
                    elif tag == 6:
                        tag = "Community"
                    elif tag == 7:
                        tag = "Other"
                    elif tag == 8:
                        tag = "Airdrop"
                    elif tag == 9:
                        tag = "AMA"
                    elif tag == 11:
                        tag = "Partnership"
                    elif tag == 13:
                        tag = "Roadmap Update"
                    elif tag == 14:
                        tag = "Fork/Swap"
                    elif tag == 15:
                        tag = "Whitepaper Update"
                    elif tag == 16:
                        tag = "Team Update"
                    elif tag == 17:
                        tag = "Farming/Stacking"
                    elif tag == 18:
                        tag = "Integration"
                    elif tag == 0:
                        tag = "No Tag"

                    params = (
                        ('id', ids.replace('\"', "", ).replace("\'", "")),
                        ('convertId', '2781'),
                        ('timeStart', '1367193600'),
                        ('timeEnd', '1642405636'),
                    )
                    responses = requests.get('https://api.coinmarketcap.com/data-api/v3/cryptocurrency/historical',
                                            headers=headers, params=params,verify=False)
                    json_response = json.loads(responses.text)
                    try:
                        symbols = json_response["data"]["symbol"]
                    except:
                        symbols = " "
                    data_currencies = {'id': id, 'Title': title, 'text': contents,
                                       'event_type': tag, 'events': tag, 'link': link,
                                       'like_count': like, 'symbol': symbols,
                                       'name': name, 'trending': trends,
                                       'confirmed': confirmed,
                                       'source': 'coinmarketcap historical', 'timestamp': times}

                    # =============================SEND TO SPARK==============================
                    # conf = SparkConf().setMaster("local[2]").setAppName("coinMarketCrawler")
                    # sc = SparkContext.getOrCreate(conf=conf)
                    # ssc = StreamingContext(sc, 1)
                    # lines = ssc.socketTextStream("localhost", 9092)
                    # tes = lines.flatMap(lambda source: source.data_currencies)
                    # do = tes.reduceByKey(lambda x,y:x+y)
                    # do.pprint()

                    # =============SEND TO KAFKA====================
                    producer.send("historical-coinmarketcap", data_currencies)
                    print(json.dumps(data_currencies))
                    # ssc.start()
                    # ssc.awaitTermination()


# data1 = json.dumps(repr(dtcr).replace('\'', '').replace('\\"', "").replace("\\", ""))
# data2 = json.dumps(repr(symb).replace('\'', '').replace('\\"', "").replace("\\", ""))
# data3 = json.dumps(repr(dctr).replace('\'', '').replace('\\"', "").replace("\\", ""))


list_brokers = ["kafka01.production02.bt:9092","kafka02.production02.bt:9092","kafka03.production02.bt:9092",
                "kafka04.production02.bt:9092","kafka05.production02.bt:9092","kafka06.production02.bt:9092"]

producer = KafkaProducer(bootstrap_servers=list_brokers,
                         value_serializer=lambda x:
                         json.dumps(x).encode("utf-8"))

# schedule.every(15).minutes.do(currencies)

if __name__ == '__main__':
    while True:
        currencies()
        # schedule.run_pending()
        time.sleep(1)
