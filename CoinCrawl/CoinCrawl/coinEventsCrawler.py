import uuid
from kafka import KafkaProducer
import json
import requests
from faker import Faker
import schedule
import time

fake = Faker()
# =====================================================
# DIBAWHA INI UNTUK CRAWLING COINMARKETCAP.COM/EVENTS
# =====================================================
def jalan():
    bagan = str(uuid.uuid4())
    headers = {
        'authority': 'api.coinmarketcap.com',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Mobile Safari/537.36',
        'content-type': 'application/json;charset=UTF-8',
        'accept': 'application/json, text/plain, */*',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
        'origin': 'https://coinmarketcap.com',
        'x-request-id': ''+bagan+'',
        'DNT': '1',
        'sec-fetch-site': 'same-site',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://coinmarketcap.com/',
        'accept-language': 'en-US,en;q=0.9',
    }

    for i in range(0, 7):
        z = str(i)
        data = '{"start":' + z + ',"limit":20,"coinType":"all","current":true}'

        response = requests.post('https://api.coinmarketcap.com/data-api/v3/calendar/query', headers=headers,
                                 data=data,verify=False)
        json_response = json.loads(response.text)

        for val in range(len(json_response["data"])):
            for x in json_response["data"][val]["eventsList"]:
                title = x["title"]
                try:
                    contents = x.get("content")
                except:
                    contents = " "
                tag = x["tags"][0]
                link = x.get("originalSource")
                like = x["like"]
                ids = x["name"][0]["id"]
                name = x["name"][0]["name"]
                trends = x["trending"]
                confirmed = x["confirmedByOfficials"]
                times = x["eventTime"]
                # # waktu = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(times/1000))
                if tag == 1:
                    tag = "Release"
                elif tag == 2:
                    tag = "Branding"
                elif tag == 3:
                    tag = "Tokenomics"
                elif tag == 4:
                    tag = "Exchange"
                elif tag == 5:
                    tag = "Conference"
                elif tag == 6:
                    tag = "Community"
                elif tag == 7:
                    tag = "Other"
                elif tag == 8:
                    tag = "Airdrop"
                elif tag == 9:
                    tag = "AMA"
                elif tag == 11:
                    tag = "Partnership"
                elif tag == 13:
                    tag = "Roadmap Update"
                elif tag == 14:
                    tag = "Fork/Swap"
                elif tag == 15:
                    tag = "Whitepaper Update"
                elif tag == 16:
                    tag = "Team Update"
                elif tag == 17:
                    tag = "Farming/Stacking"
                elif tag == 18:
                    tag = "Integration"

                params = (
                    ('id', ids.replace('\"', "", ).replace("\'", "")),
                    ('convertId', '2781'),
                    ('timeStart', '1367193600'),
                    ('timeEnd', '1642405636'),
                )
                responses = requests.get('https://api.coinmarketcap.com/data-api/v3/cryptocurrency/historical',
                                        headers=headers, params=params,verify=False)
                json_responses = json.loads(responses.text)
                try:
                   symbols = json_responses["data"]["symbol"]
                except:
                    symbols = " "

                data_coin = {'id': ids, 'title': title, 'text': contents, 'event_type': tag, 'events': tag,
                             'link': link, 'like_count': like, 'symbol': symbols, 'name': name,
                             'trending': trends,
                             'confirmed': confirmed, 'source': 'coinmarketcap',
                             'timestamp': times}

                producer.send("event-cyrpto", data_coin)
                print(json.dumps(data_coin))


# data1 = json.dumps(repr(dtcr).replace('\'', '').replace('\\"', "").replace("\\", ""))
# data2 = json.dumps(repr(symb).replace('\'', '').replace('\\"', "").replace("\\", ""))
# data3 = json.dumps(repr(dctr).replace('\'', '').replace('\\"', "").replace("\\", ""))

list_brokers = ["kafka01.production02.bt:9092","kafka02.production02.bt:9092","kafka03.production02.bt:9092",
                "kafka04.production02.bt:9092","kafka05.production02.bt:9092","kafka06.production02.bt:9092"]


producer = KafkaProducer(bootstrap_servers=list_brokers,
                         value_serializer=lambda x:
                         json.dumps(x).encode("utf-8"))

# schedule.every(15).minutes.do(jalan)

if __name__ == '__main__':
    while True:
        jalan()
        # schedule.run_pending()
        time.sleep(1)

