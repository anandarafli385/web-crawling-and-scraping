# INI DAFTAR AKUN BUAT AMBIL APINYA
from bs4 import BeautifulSoup as bSoup
from kafka import KafkaProducer
import requests
import json
import schedule
import time
from faker import Faker

fake = Faker()

def market():

    for i in range(1, 45):
        cookies = {
            'device_view': 'full',
            '_ga_90JJS7QB1F': 'GS1.1.1643368664.4.0.1643368664.0',
            '_ga': 'GA1.1.1105524145.1640874961',
            '_fbp': 'fb.1.1640874962031.565010702',
            'AWSALB': '/Jd6+fYi6AoB/3rIk/S6rVoCZvVeDIWJoYS23ZEtlBSJoGbIAlCy8FpUOMP5jZ8KySEbZ7j0w6pH/5NQd6p+KZt28NY80yWcj/4+hClH58nTweNKEiDKuBt+W+32',
            'AWSALBCORS': '/Jd6+fYi6AoB/3rIk/S6rVoCZvVeDIWJoYS23ZEtlBSJoGbIAlCy8FpUOMP5jZ8KySEbZ7j0w6pH/5NQd6p+KZt28NY80yWcj/4+hClH58nTweNKEiDKuBt+W+32',
            'PHPSESSID': 'p623spk7o4vnmvbjkg83vautjf',
        }

        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'cross-site',
            'Cache-Control': 'max-age=0',
            'TE': 'trailers',
        }

        params = {
            'page': i
        }

        response = requests.get('https://coinmarketcal.com/en/', headers=headers, cookies=cookies, params=params)
        soup = bSoup(response.text, 'lxml')

        cards = soup.find_all('article',class_='col-xl-3 col-lg-4 col-md-6 py-3')

        for i in cards:
            for a in i.find_all('a',class_='link-detail', href=True):
                idd = a['href'].split(',')
                if  "/event/" in idd[0]:
                    isi = idd[0].split('-')
                    ids = isi[len(isi)-1]


                    title = i.find('h5',class_='card__title mb-0 ellipsis').text
                    name = i.find('a',class_='link-detail').text.replace(" and 1","").replace("other","").replace("\n","").replace("                                            ","")
                    dates = i.find('h5',class_='card__date mt-0').text

                    symbols = i.find('a',class_='link-detail').text.replace(" and 1","").replace("other","").replace("\n","").split("(")
                    symbs = symbols[1].split(")")
                    symb = symbs[0]
                    link = i.find('a',class_='btn btn-sm btn-border-b btn-color-link rounded-pill btn-block', href=True).get('href')
                    text = i.find('p', class_='card__description').text.replace("\n","").replace("                                            ","").replace("                                    ","")
                    like = i.find('div', class_='progress__votes').text.replace("\n","").replace("                        Votes","").replace("                        Vote","")
                    try:
                        if i.find('i', class_='icon-level-up'):
                            trending = True
                        else:
                            trending = False
                    except:
                        trending = False
                    try:
                        if i.find('i', class_='icon-badge-check'):
                            confirmed = True
                        else:
                            confirmed = False
                    except:
                        confirmed = False

                    market_data = {'id': ids, 'title': title, 'text': text, 'link': link,
                                   'like_count': like, 'symbols': symb, 'name': name, 'trending': trending,
                                   'confirmed': confirmed, 'source': 'coinmarketcal', 'timestamp': dates}
    #
    #         # market = json.dumps(market_data)
    #         # mrkt.append(market_data)
                    producer.send("test-crypto", market_data)
                    # producer.send("coinmarketcal", market_data)
                    print(market_data)


# list_brokers = ["kafka01.production02.bt:9092","kafka02.production02.bt:9092","kafka03.production02.bt:9092",
#                 "kafka04.production02.bt:9092","kafka05.production02.bt:9092","kafka06.production02.bt:9092"]


producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                             value_serializer=lambda x :
                             json.dumps(x).encode("utf-8"))

# schedule.every(15).minutes.do(market)

if __name__ == '__main__':
    while True:
        market()
        # schedule.run_pending()
        time.sleep(1)
